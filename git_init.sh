#!/bin/bash
set -x
rm -rf .git
git init
git add .
git commit -m "reset"
git remote add origin git@gitlab.com:st0n3/docker_build_test.git
git push -f -u origin master
